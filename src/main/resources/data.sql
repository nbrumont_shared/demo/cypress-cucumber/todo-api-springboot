DROP TABLE IF EXISTS todo;

CREATE TABLE todo (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  label VARCHAR(250) NOT NULL,
  completed BOOLEAN DEFAULT FALSE
);

INSERT INTO todo (label, completed) VALUES
  ('Aller courir', false),
  ('Présenter Cypress', true),
  ('Faire la vaiselle', false);