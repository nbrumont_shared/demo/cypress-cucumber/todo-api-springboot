package fr.nbrumont.todoapi.todo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface TodoRepository extends CrudRepository<Todo, Long> {
    @Override
    List<Todo> findAll();
}
